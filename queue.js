let collection = [];

// Write the queue functions below.

//!  [PRINT QUEUE ELEMENTS]
function print() {
    return collection;
  }
  
//! [ENQUEUE]
  function enqueue(name) {
    collection.push(name);
    return collection;
  }
 
  
//! [DEQUEUE]
  function dequeue() {
    collection.shift();
    return collection;
  }
  
//! [FRONT]
  function front() {
    return collection[0];
  }
  
//! [QUEUE SIZE]
  function size() {
    return collection.length;
  }
  
//! [isEmpty]
  function isEmpty() {
    if (collection.length === 0) {
      return true;
    } else {
      return false;
    }
  }
  
  module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
  };



